terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 3.0.1"
    }
  }
}

provider "docker" {
  host = "npipe:////.//pipe//docker_engine"
}

resource "docker_image" "example-be" {
  name = "example-be"
  build {
    context = "./example-back-end"
    tag     = ["example-be"]
  }
}

resource "docker_image" "example-fe" {
  name = "example-fe"
  build {
    context = "./example-front-end"
    tag     = ["example-fe"]
  }
}

resource "docker_container" "run-example-be" {
  image = docker_image.example-be.image_id
  name  = "back-end"
  ports {
    internal = 8080
    external = 81
  }
}

resource "docker_container" "run-example-fe" {
  image = docker_image.example-fe.image_id
  name  = "front-end"
  ports {
    internal = 8080
    external = 80
  }
}