# Terraform Example Running

- `terraform init` init terraform at beginning
- `terraform plan` to seeing what plan/job that we will run
- `terraform apply` to run the plan/program, then write `yes`
- `terraform destroy` to terminates resources managed by your Terraform project, then write `yes`
