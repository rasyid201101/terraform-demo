package com.tujuhsembilan.test.configuration.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Screens {

  TEST("TEST", "test")

  ;

  private final String name;
  private final String grants;

  public static Screens getByName(String name) {
    for (Screens screen : values()) {
      if (screen.name.equals(name)) {
        return screen;
      }
    }

    throw new IllegalArgumentException("Screen by name { " + name + " } doesn't exists");
  }

}
