package com.tujuhsembilan.test.configuration.service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.tujuhsembilan.lib.i18n.utility.MessageUtil;
import com.tujuhsembilan.test.configuration.property.SecurityProp;
import com.tujuhsembilan.test.model.Users;
import com.tujuhsembilan.test.repository.UsersRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppUserDetailsSrvc implements UserDetailsService {

  private final MessageUtil message;

  private final UsersRepository userRepository;

  private final SecurityProp securityProp;
  private final PasswordEncoder passwordEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    if (!securityProp.getDefaultLogin().equals(username)) {
      Optional<Users> user = userRepository.findOneByUserName(username);

      if (!user.isPresent()) {
        throw new UsernameNotFoundException(message.get("system.generic.not-found-error", username));
      }

      return user.get();
    } else {
      return new UserDetails() {
        @Override
        public String getUsername() {
          return securityProp.getDefaultLogin();
        }

        @Override
        public String getPassword() {
          return passwordEncoder.encode(securityProp.getDefaultPassword());
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
          return Set.of(() -> "SYSTEM_ADMIN");
        }

        @Override
        public boolean isAccountNonExpired() {
          return true;
        }

        @Override
        public boolean isAccountNonLocked() {
          return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
          return true;
        }

        @Override
        public boolean isEnabled() {
          return true;
        }
      };
    }
  }

}
