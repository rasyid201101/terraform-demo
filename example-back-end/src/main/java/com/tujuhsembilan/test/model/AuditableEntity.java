package com.tujuhsembilan.test.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditableEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column
  @CreatedBy
  private Long createdBy;

  @Column
  @CreatedDate
  private LocalDateTime createdTime;

  @Column
  @LastModifiedBy
  private Long lastModifiedBy;

  @Column
  @LastModifiedDate
  private LocalDateTime lastModifiedTime;

}
