package com.tujuhsembilan.test.model.embed;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class RoleKey implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "screen_id")
	private Long screenId;

	@Column(name = "user_type_id")
	private Long userTypeId;

}
