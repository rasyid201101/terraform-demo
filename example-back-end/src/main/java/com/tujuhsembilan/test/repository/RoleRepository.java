package com.tujuhsembilan.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tujuhsembilan.test.model.Role;
import com.tujuhsembilan.test.model.embed.RoleKey;

public interface RoleRepository extends JpaRepository<Role, RoleKey> {
}
