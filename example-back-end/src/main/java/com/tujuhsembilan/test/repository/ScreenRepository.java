package com.tujuhsembilan.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tujuhsembilan.test.model.Screen;

public interface ScreenRepository extends JpaRepository<Screen, Long> {
}
