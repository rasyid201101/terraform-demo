package com.tujuhsembilan.test.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tujuhsembilan.test.model.Users;

public interface UsersRepository extends JpaRepository<Users, Long> {

  public Optional<Users> findOneByUserName(String userName);

}
