package com.tujuhsembilan.test.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tujuhsembilan.test.security.dto.RegistrationRequestBody;
import com.tujuhsembilan.test.security.service.AuthService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthController {

  private final AuthService svc;

  @PostMapping("/register")
  public Object register(@RequestBody RegistrationRequestBody requestBody) {
    return svc.register();
  }

  @PostMapping("/login")
  public Object login(Authentication requestBody) {
    return svc.login(requestBody);
  }

}
