package com.tujuhsembilan.test.security.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationRequestBody {

  private String nip;
  private String name;

  private String userName;
  private String email;
  private String password;

}
