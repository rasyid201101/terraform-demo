package com.tujuhsembilan.test.security.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;

import com.tujuhsembilan.test.security.dto.LoginResponseBody;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuthService {

  private final JwtEncoder encoder;

  public Object register() {
    return null;
  }

  public Object login(Authentication requestBody) {
    long maxAge = 36000L;

    Instant now = Instant.now();
    Instant expiry = now.plusSeconds(maxAge);

    String scope = requestBody
        .getAuthorities().stream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.joining(" "));

    // @formatter:off
		JwtClaimsSet claims = JwtClaimsSet
      .builder()
        .issuer("self")
        .issuedAt(now)
        .expiresAt(expiry)
        .subject(requestBody.getName())
        .claim("scope", scope)
      .build();
		// @formatter:on

    return LoginResponseBody.builder()
        .accessToken(encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue())
        .issuedAt(LocalDateTime.ofInstant(now, ZoneOffset.UTC))
        .expiredOn(LocalDateTime.ofInstant(expiry, ZoneOffset.UTC))
        .subject(requestBody.getName())
        .build();
  }

}
